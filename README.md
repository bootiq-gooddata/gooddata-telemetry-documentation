# GoodData Telemetry Extractor
This extractor uses GoodData API for getting info about users, projects, dashboards, reports and uploaded datasets

## Running the extractor
For this extractor you need account in a project or multiple projects in GoodData.

### Config:
* **Login email**
* **Password**
* **The white-labeled domain** must be with a region and without the dot in the end. Eg.: `bootiq.na`, `keboola.eu`, `secure`

If you do not have White-labeled domain, then use `secure` domain, which is default domain for GoodData.

### Output

On the output you can find 5 tables:

* `users`
* `projects`
* `dashboards`
* `reports`
* `datasets`
* `uploads` -> This one uses increment loading but does not have primary key so you need to delete duplicated rows

  
#### Data  Model

```mermaid
graph TD

projects -- project_id --> users
projects -- project_id --> dashboards
users -- user_id --> dashboards
users -- user_id --> reports
users -- user_id --> datasets
projects -- project_id --> reports
projects -- project_id --> datasets
datasets -- data_uploads --> uploads

```

#### Easy python script to delete duplicated rows

```python
import pandas as pd
  

#load data
df = pd.read_csv("in/tables/uploads.csv")

#drop duplicates
df.drop_duplicates(inplace = True)
  
#save data || It is recommended to have the same table at input and output mapping
df.to_csv("out/tables/uploads.csv", index=False)
```
